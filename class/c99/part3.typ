#import "../../template/ytemplate.typ": *


#ypart[Le problème #smallcaps[Sat]]


#ydef(name: [Table de vérité d'une formule])[
    La table de vérité d'une formule $H in cal(F)$
    est un tableau ou chaque ligne correspond à un
    environnement propositionnel et chaque colonne
    à un sous-formule de $H$ #footnote[Qui n'est
    pas $top$ ou $bot$]. À l'intersection des
    deux on écrit l'interprétation sémantique que
    la sous-formule dans l'environnement propositionnel.

    E.g. $(p and q) -> r$ donne
    #align(center, table(
        columns: 5,
        [$p$], [$q$], [$r$], [$p and q$], [$(p and q) -> r$],
        [$F$], [$F$], [$F$], [$F$], [$V$],
        [$F$], [$F$], [$V$], [$F$], [$V$],
        [$F$], [$V$], [$F$], [$F$], [$V$],
        [$dots.v$], [$dots.v$], [$dots.v$], [$dots.v$], [$dots.v$],
    ))
]


#ydef(name: [Problème #smallcaps[Sat].])[
    - _Entrée_ : Une formule $H in cal(F)$.
    - _Sortie_ : $H$ est elle satisfiable ?

    On pourrait aussi renvoier un modèle de $H$
    si $H$ est satisfiable et _None_ sinon.
]

#ydef(name: [Problème #smallcaps[Valide].])[
    - _Entrée_ : Une formule $H in cal(F)$.
    - _Sortie_ : $H$ est elle valide ?

    On pourrait aussi renvoier un environnement
    propositionnel $mu$ tel que $[|H|]^mu = F$
    ou _None_ sinon.
]

#ydef(name: [Représentation des formules dans les problèmes #smallcaps[Sat] et #smallcaps[Valide].])[
    Les formules dans ces les problèmes #smallcaps[Sat] et
    #smallcaps[Valide] sont représentées sous la forme d'arbres.

    Si les formules était données sous forme de tables de vérités
    ils suffirait de lire la dernière colonne pour résoudre le problème.
]