#import "../../template/ytemplate.typ": *


#ypart[Représentation des fonctions booléennes]


#ydef(name: [Table de vérité d'une fonction booléenne])[
    De manière générale, lorsque $cal(P)$ est fini et
    contient $n$ éléments, la table de vérité
    d'une fonction booléenne $f : BB^cal(P) → BB$
    est un tableau à $2^n + 1$ lignes et $n + 1$ colonnes.
    
    Excepté pour la première ligne qui est utilisée
    pour donner des noms, la lecture d'une ligne du
    tableau commence donc par $n$ colonnes formant
    un environnement booléen $mu$ et se termine par
    une colonne indiquant la valeur de $f(mu)$.

    Si $P$ n'est pas fini, sa définition par table
    de vérité n'est pas possible.
]

#ytheo(name: [Exhaustivité de la logique propositionnelle])[
    Pour toute fonction booléenne $f$
    sur un ensemble de variables propositionnelles
    $cal(P)$ fini, il existe une formule de la
    logique propositionnelle $H$ telle que $[|H|] = f$
    (et on a un algorithme).
]

#yproof[
    On note $cal(P) = { p_1, ..., p_n }$ \
    et pour chaque $mu in BB^cal(P)$ et $p in cal(P)$,
    $ "lit"_mu (p) = cases(p "si" mu(p) = V, not p "sinon") $

    On pose, pour $mu in BB^cal(P)$,
    $ H_mu = and.big_(i=1)^n "lit"_mu (p_i) $
    (avec si $n=0$, $and.big_(i=1)^n ... = top$)

    - #underline(smallcaps[Lemme])
        $forall mu in BB^cal(P), forall rho in BB^cal(P)$
        $ [|H_mu|]^rho <=> mu = rho $

        #underline[preuve]
        - _Mq_ $forall mu in BB^cal(P), [|H_mu|]^mu = V$.
        - _Mq_ $forall mu != rho in BB^cal(P), [|H_mu|]^rho = F$.
    
    On pose
    $ G = or.big_(mu in cal(P) \ f(mu) = V) H_mu $

    _Mq_ $[|G|] = f$.
]

#ydef(name: [Clause disjonctive / conjonctive])[
    On appelle _clause disjonctive_ (resp. _conjontive_)
    une disjonction #footnote[éventuellement vide] <evnt-vide>
    (resp. conjonction @evnt-vide) de littéraux.
]

#ydef(name: [F.N.C / F.N.D.])[
    On appelle _forme normale conjonctive_ (resp. _disjonctive_)
    une conjonction #footnote[éventuellement vide] <evnt-vide2>
    (resp. disjonction @evnt-vide2) de clauses disjonctives
    (reps. conjonctives).
]

#ytheo(name: [Éxistance d'une F.N.C. et d'une F.N.D. représentant une fonction booléenne])[
    Pour toute fonction booléenne $f$,
    il existe une formule $G$ sous F.N.C.
    et une formule $H$ sous F.N.D. telles
    que $[|G|] = [|H|] = f$
    (et on a un algorithme).
]

#ymetho(name: [Satisfiabilité d'une F.N.D.])[
    Soit $F = or.big_(i=1)^n C_i$ une F.N.D.

    Pour satisfaire $F$, il suffit de satisfaire l'une
    des clauses conjonctive $C_i$.

    La clause conjonctive $C_i$ est
    - insatisfiable s'il existe $p in cal(P)$ telle que
        $ p in C_i "et" not p in C_i $
    - sinon satisfiable par
        $ mu(p) = cases(V "si" p in C_i, F "sinon") $
        (les cas « sinon » incluent les cas où
        $not p in C_i$)
    
    On a alors un algorithme de complexité linéaire
    (en taille de la F.N.D.).

    Rappelons toutefois que la taille de la F.N.D. obtenue peut
    être de $2^n$ où $n$ est la taille de la formule
    initiale.
]

#ydef(name: [Substitution])[
    Soient $H$, $G$ deux formules
    et $p$ une variable propositionnelle.

    On définit inductivement la substitution
    $H[p := G]$ par

    $ top[p := G] &= top \
        bot[p := G] &= med bot \
        q[p := G] &= q wide "si" q != p \
        p[p := G] &= G \
        (not G_1)[p := G] &= not (G_1 [p := G]) \
        (G_1 dot.circle G_2)[p &:= G] \
            = G_1 [p &:= G] dot.circle G_2 [p := G] $
    avec $dot.circle in {and, or, ->, <->}$.
]

#ylemme(name: [Dans le cas où $mu(p) = V$, lien entre $H$ et $H[p := top]$])[
    Soit $H in cal(F)$, $p in cal(P)$ et $mu in BB^cal(P)$
    tels que $mu(p) = V$ (resp. $mu(p) = F$).

    Alors $[|H[p := top]|]^mu = [|H|]^mu$ \
    (resp. $[|H[p := bot]|]^mu = [|H|]^mu$).
]

#yproof[
    Par induction.
]

#ylemme(name: [Satisfiabilité d'une formule dans laquelle n'apparait aucune variable propositionnelle])[
    Soit $G in cal(F)$ telle que $"vars"(G) = emptyset$.

    Alors $G equiv top$ (satisfiable) \
    ou $G equiv med bot$ (insatisfiable) \
    (et on a un algorithme).
]

#yproof[
    Par induction.
]

#ydef(name: [Vision ensembliste d'une forme normale])[
    Une forme normale peut être représentée comme un
    ensemble d'ensemble de littéraux.

    Par exemple, la F.N.C.
    $ &(p or q or p) \
        &and (q or p) \
        &and (p or not q) \
        &and (q or not q) $
    devient
    $ { {p, q}, {p, not q}} $

]

#yalgo(name: [Fonction assume de l'algorithme de Quine])[
    #algorithm(
        caption: [assume],
        pseudocode-list[
            - *Entrées:*
                - $G in cal(F)$ une F.N.C. ensembliste
                - $p in cal(P)$ une var. prop.
                - $b in BB$ un booléen
            - *Sortie:* Une formule équivalente à
                $G[p := top]$ si $b = V$ ou \
                $G[p := med bot]$ sinon

            + $ell_V <- p$ *si* $b = V$ *sinon* $not p$
            + $ell_F <- not p$ *si* $b = V$ *sinon* $ p$
            + *pour tout* $C in G$ *faire*
                + *si* $ell_V in C$ *alors*
                    + *supprimer* $C$ de $G$
                + *si* $ell_F in C$ *alors*
                    + *supprimer* $ell_F$ de $C$
            + *retourner* $C$
        ]
    )
]

#yalgo(name: [Algorithme de Quine])[
    #algorithm(
        caption: [quine],
        pseudocode-list[
            - *Entrées:* $G in cal(F)$ une F.N.C. ensembliste
            - *Sortie:* $G$ est-elle satisfiable 

            + *si* $G = emptyset$ *alors*
                +  Vrai
            + *sinon si* $emptyset in G$ *alors*
                + False
            + *sinon si* $exists p in cal(P), {p} in G$ *alors*
                + $"quine"("assume"(G, p, V))$
            + *sinon si* $exists p in cal(P), {not p} in G$ *alors*
                + $"quine"("assume"(G, p, F))$
            + *sinon*
                + $p <- h(G)$
                + $"quine"("assume"(G, p, V))$
                    *ou* $"quine"("assume"(G, p, F))$
        ]
    )
    $h$ est une fonction d'_heurestique_
    à déterminer, qui permet la sécection d'une des
    variables de la clause.
]