#import "../../template/ytemplate.typ": *


#ypart[Syntaxe]


#ydef(name: [Ensemble des variables propositionnelles])[
    Nous supposons donné un ensemble $cal(P)$ et appelons
    _variables propositionnelles_ les éléments de cet ensemble.
]

#ydef(name: [Formule de la logique propositionnelle])[
    Étant donné un ensemble $cal(P)$ de variables propositionnelles,
    on définit l'_ensemble des formules_ $cal(F)$ par l'induction
    structurelle suivante :
    - $bot in cal(F)$
    - $top in cal(F)$
    - $p in cal(F)$
    - $not G in cal(F)$
    - $G and H in cal(F)$
    - $G or H in cal(F)$
    - $G -> H in cal(F)$
    - $G <-> H in cal(F)$
    pour tout $(G, H) in cal(F)^2$ et $p in cal(P)$.
]

#ydef(name: [Taille d'une formule])[
    On définit par induction structurelle une fonction
    $"taille" : cal(F) -> N$ (ici abregée en $frak(t)$).
    $ frak(t): cases(
            top      &|-> 1,
            bot      &|-> 1,
            p        &|-> 1,
            not G    &|-> 1 + frak(t)(G),
            G or H   &|-> 1 + frak(t)(G) + frak(t)(H),
            G and H  &|-> 1 + frak(t)(G) + frak(t)(H),
            G -> H   &|-> 1 + frak(t)(G) + frak(t)(H),
            G <-> H  &|-> 1 + frak(t)(G) + frak(t)(H)) $
    pour $(G, H) in cal(F)^2$ et $p in cal(P)$.
]

#ydef(name: [Ensemble des variables propositionnelles d'une formule])[
    On définit par induction structurelle une fonction
    $"vars" : cal(F) → cal(p)(cal(P))$ (ici abregée en $frak(v)$).
    $ frak(v): cases(
            top      &|-> emptyset,
            bot      &|-> emptyset,
            p        &|-> {p},
            not H    &|-> frak(v)(G),
            G or H   &|-> frak(v)(G) union frak(v)(H),
            G and H  &|-> frak(v)(G) union frak(v)(H),
            G -> H   &|-> frak(v)(G) union frak(v)(H),
            G <-> H  &|-> frak(v)(G) union frak(v)(H)) $
]

#ydef(name: [Littéral])[
    Si $cal(P)$ est un ensemble de variables propositionnelles,
    on note $cal(L)(cal(P))$ l'_ensemble des littéraux_, définis comme
    $ cal(L)(cal(P)) = {not p | p in cal(P)} union cal(P) $
    On dira d'un littéral $p$ qu'il est un _littéral positif_,
    et d'un littéral $not p$ que c'est un _littéral négatif_.
]

#ydef(name: [Sous-formules])[
    On appelle _sous-formule d'une formule $H$_ une formule $G$
    telle que $G prec.eq H$ où $prec.eq$ est la relation
    d'ordre bien fondée induite par la définition inductive
    des formules.

    Les sous-formules sont des sous-termes au sens de la
    définition inductive.
]