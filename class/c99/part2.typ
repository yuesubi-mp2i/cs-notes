#import "../../template/ytemplate.typ": *


#ypart[Sémentique]


=== 1. Algèbre de Boole

#ydef(name: [Ensemble des booléens])[
    Dans toute la suite on note $BB = {V, F}$
    un ensemble à deux éléments nommés _booléens_.
]

#ydef(name: [$+$, $dot$ et $overline(square)$])[
    Sur l'espace $BB$, on définit les trois opérateurs suivants

    #align(center, table(
        columns: 3, stroke: none, inset: 0.5em,
        table(
            columns: 3, inset: 0.3em,
            [$+$], [$F$], [$V$],
            [$F$], [$F$], [$V$],
            [$V$], [$V$], [$V$]
        ),
        table(
            columns: 3, inset: 0.3em,
            [$+$], [$F$], [$V$],
            [$F$], [$F$], [$F$],
            [$V$], [$F$], [$V$]
        ),
        table(
            columns: 3, inset: 0.3em,
            [$overline(square)$], [$F$], [$V$],
            [$$], [$V$], [$F$],
        )
    ))
]

#yprop(name: [Propriétés élémentaires du _ou_ sémantique])[
    - commutativité \
        $forall a,b in BB, a + b = b + a$
    - associativité \
        $forall a,b, c in BB, (a + b) + c = a + (b + c)$
    - idempotence \
        $forall a in BB, a + a = a$

    - élément neutre \
        $forall a in BB, a + F = a$
    - élément absorbant \
        $forall a in BB, a + V = V$
]

#yprop(name: [Propriétés élémentaires du _et_ sémantique])[
    - commutativité \
        $forall a,b in BB, a dot b = b dot a$
    - associativité \
        $forall a,b, c in BB, (a dot b) dot c = a dot (b dot c)$
    - idempotence \
        $forall a in BB, a dot a = a$

    - élément neutre \
        $forall a in BB, a dot V = a$
    - élément absorbant \
        $forall a in BB, a dot F = F$
]

#yprop(name: [Distributivité entre le $+$ et le $dot$ booléen])[
    Pour tout $a, b, c in BB$,
    $ cases(a dot (b + c) &= a dot b + a dot c,
        a + (b dot c) &= (a + b) dot (a + c)) $
]

#yprop(name: [Propriétés des lois booléennes $+$ et $dot$ avec le $overline(square)$])[
    - complément :
        $ forall a in BB, cases(a dot overline(a) &= F, a + overline(a) &= V) $
    - lois de De Morgan
        $ forall a, b in BB,
            cases(overline(a dot b) &= overline(a) + overline(b),
                overline(a + b) &= overline(a) dot overline(b)) $ 
]

=== 2. Fonction booléennes

#ydef(name: [Environnement propositionnel])[
    Étant donné un ensemble de variables propositionnelles $cal(P)$,
    un _environnement propositionnel_ $mu in BB^cal(P)$ est une fonction
    de $cal(P)$ dans $BB$.
]

#ynot(name: [Représentation d'un environnement propositionnel])[
    Lorsque $cal(P) = {p_1, ..., p_n}$ est un ensemble fini,
    on adopte la notation
    $ (p_1 |-> b_1, ..., p_n |-> b_n) $
    pour représenter l'environnement propositionnel ci-dessous
    $ mu : &cal(P) -> BB \
        &p_i |-> b_i "où" i in [|1, n|] $
]

#ydef(name: [Fonctions booléennes])[
    Étant donné un ensemble de variables propositionnelles $cal(P)$,
    l'_ensemble des fonctions booléennes sur $cal(P)$_, noté $cal(F)(cal(P))$,
    est l'ensemble des fonctions $f : BB^cal(P) -> BB$.
    $ cal(F)(cal(P)) = BB^(BB^cal(P)) $
]

=== 3. Interprétation d'une formule comme une fonction booléenne

#ydef(name: [Interprétation d'une formule dans un environnement])[
    Étant donnés un ensemble de variables $cal(P)$
    et une formule de $cal(F)$ on définit inductivement
    l'_interprétation d'une formule $H$ dans un environnement
    booléen $mu$_, noté $[|H|]^mu$ par :
    $ [|top|]^mu &:= V \
        [|bot|]^mu &:= F \
        [|p|]^mu &:= mu(p) \
        [|not G|]^mu &:= overline([|G|]^mu) \
        [|G and H|]^mu &:= [|G|]^mu dot [|H|]^mu \
        [|G or H|]^mu &:= [|G|]^mu + [|H|]^mu \
        [|G -> H|]^mu &:= overline([|G|]^mu) + [|H|]^mu \
        [|G <-> H|]^mu &:= cases(V "si" [|G|]^mu = [|H|]^mu, F "sinon") $
    pour $(G, H) in cal(F)^2$ and $p in cal(P)$.
]

#ydef(name: [Fonction booléenne associée à une formule])[
    À une formule $H in cal(F)$ de la logique propositionnelle
    on associe une fonction booléenne $[|H|]$ de la manière suivante :
    $ [|H|]: BB^cal(P) &-> BB \
        mu &|-> [|H|]^mu $
]

#ylemme(name: [Condition suffisante sur les environnements propositionnels pour que l'interprétation d'une formule donnée soit la même])[
    Soit $H$ une formule de la logique propositionnelle.

    Soit $mu$ et $rho$ deux environnements propositionnels
    tels que $mu_(|"vars"(H)) = rho_(|"vars"(H))$ alors
    $ [|H|]^mu = [|H|]^rho$.
]

#yproof[
    Par induction structurelle. $qed$
]

=== 4. Liens sémantiques entre formules

#ydef(name: [Formules équivalentes])[
    Deux formules $H$ et $G$ sont dites équivalentes,
    ce que l'on note $H equiv G$, dès lors que $[|H|] = [|G|]$.
]

#yprop(name: [Substitution d'une formule par une autre équivalente])[
    On a en fait que si $G equiv H$ alors :
    - $not G equiv not H$
    - $G and K equiv H and K$
    - $G or K equiv H or K$
    - $G -> K equiv H -> K$
    - $G <-> K equiv H <-> K$
    avec $H in cal(F)$.
]

#yproof[
    Par induction structurelle immédiate. $qed$
]

#ylemme(name: [Particularité d'une formule sans variables])[
    Soit $H$ une formule telle que $"vars"(H) = emptyset$,
    alors $H equiv top$ ou $H equiv bot$.
]

#yproof[
    Par induction structurelle immédiate. $qed$
]

#ydef(name: [Conséquence sémantique])[
    On dit qu'une formule $G in cal(F)$ est conséquence sémantique
    d'une autre formule $H in cal(F)$, et on note alors
    $H tack.r.double G$, dès lors que : pour $mu in BB^cal(P)$,
    $ [|H|]^mu = V => [|G|]^mu = V $
]

#ynot(name: [Conséquence sémantique d'un ensemble de formules])[
    Si $Gamma$ est un ensemble de formules, on dénote par
    $Gamma tack.r.double G$ le fait que pour tout $mu in BB^cal(P)$,
    $ (forall H in Gamma, [|H|]^mu = V) => ([|G|]^mu = V) $
]

#yprop(name: [Double conséquence sémentique])[
    Pour toutes formules $H$ et $G$ de la logique propositionnelle,
    $H equiv G$ ssi $H tack.r.double G$ et $G tack.r.double H$.
]

#yproof[
    - "$==>$" Soit $H$ et $G$ deux formules telles que $H equiv G$,
        ainsi $[|H|] = [|G|]$.
        
        Soit $mu in BB^cal(P)$, alors $[|H|]^mu = [|G|]^mu$.
        
        Montrons alors que $H tack.r.double G$.
        
        Soit $mu in BB^cal(P)$ tel que $[|H|]mu = V$,
        alors $[|G|]^mu = [|H|]^mu = V$.
        
        De même on montre que $G tack.r.double H$.

    - "$<==$" Soit $H$ et $G$ deux formules telles que $H tack.r.double G$
        et $G tack.r.double H$.
        
        Soit $mu in BB^cal(P)$ un environnement propositionnel.
        - Si $[|G|]^mu = V$ alors de $G tack.r.double H$, $[|H|]^mu = V$

        - Sinon (si $[|G|]^mu = F$) alors par contraposée de
            $H tack.r.double G$, $[|H|]^mu = F$.
]

#ydef(name: [Formules valides, modèles, satisfiabilité])[
    On dit d'une formule $H in F$ et d'un
    environnement propositionnel $mu$ que :
    - $H$ est _valide_, ou est une _tautologie_,
        lorsque pour $mu in BB^cal(P)$, $[|H|]^mu = V$.
    - $H$ est _satisfiable_, lorsqu'il existe $mu in BB^cal(P)$ tel que
        $[|H|]^mu  = V$.
    - $H$ est _insatisfiable_ ou est dite _antilogie_,
        lorsque $H$ n'est pas satisfiable.
    - $mu$ est un modèle de $H$ lorsque $[|H|]^mu  = V$.
]

#yprop(name: [Lien entre conséquence sémentique et syntaxique])[
    Soit $H$ et $G$ deux formules de $cal(F)$.

    $H tack.r.double G$ ssi $H -> G$ est valide.
]

#yproof[
    $H tack.r.double G$ \
    ssi $forall mu in BB^cal(P), [|H|]^mu  = V => [|G|]^mu  = V$ \
    ssi $forall mu  in BB^cal(P), [|H|]^mu  = F "ou" [|G|]^mu  = V$ \
    ssi $forall mu  in BB^cal(P), ([|H|]^mu  + [|G|]^mu ) = V$ \
    ssi $forall mu  in BB^cal(P), [|H -> G|]^mu  = V$ \
    ssi $H -> G$ est valide.
]

#yprop(name: [Lien entre conséquence sémentique et syntaxique d'un ensemble de formules])[
    Soit $Gamma$ est un ensemble fini de formules.

    $(and.big_(H in Gamma) H) -> G$ est valide ssi
    $Gamma tack.r.double G$.
]

#yprop(name: [Conséquence sémantique avec une cause insatisfiable])[
    Soient $H$ et $G$ deux formules de $cal(F)$.

    Si $H$ est insatisfiable, alors pour toute formule $G$
    on a $H tack.r.double G$.
]

#yprop(name: [Caractérisation de la conséquence sémentique par une formule insatisfiable])[
    Soient $H$ et $G$ deux formules de $cal(F)$.

    $H tack.r.double G$ ssi $H and not G$ est insatisfiable.
]