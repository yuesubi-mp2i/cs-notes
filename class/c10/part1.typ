#import "../../template/ytemplate.typ": *


#ypart[Tout, mal organisé]


#let tree(data, length) = {
  cetz.canvas(length: length, {
    import cetz: *
    import cetz.draw: *

    set-style(content: (padding: .2),
      // fill: gray.lighten(70%),
      stroke: (paint: gray.lighten(70%), thickness: 0.06em))

    tree.tree(data, spread: 2.5, grow: 1.5, draw-node: (node, ..) => {
      circle((), radius: .45, stroke: none)
      content((), node.content)
    }, draw-edge: (from, to, ..) => {
      line((a: from, number: .5, b: to),
           (a: to, number: .5, b: from))
    }, name: "tree")
  })
}



#ydef(name: [Arbre $n$-aire])[
  Un arbre $A$ est un ensemble de $n >= 1$ noeuds tels que
  + un noeud particulier $r$ est appelé _racine_ de l'arbre ;
  + les $n - 1$ noeuds restants sont partitionnées en $k >= 0$
    sous-ensembles disjoints qui forment les _sous-arbre_ de $r$ ;
  + la racine $r$ est liée à la racine de chaqu'un des $k$
    sous-arbres par un _arc_ ou une _arête_.

  #align(center)[
    #figure(
      tree(($A$, ($B$, $D$), ($C$, $E$, $F$, ($G$, $H$))), 1.2em),
      supplement: [Arbre],
      caption: [Exemple]
    )
  ]
]

#ydef(name: [Forêt d'un arbre])[
  On appelle _forêt_ l'ensemble des sous-arbres d'un noeud.

  N.B. Une forêt peut être vide.
]

#ydef(name: [Arité d'un noeud d'arbre])[
  L'_arité_ d'un noeud est défini comme son nombre de succésseurs.

  Si un noeud est d'arité $n$, on dit que cet un noeud $n$-aire.
]

#ydef(name: [Ancêtre d'un noeud])[
  On appelle _ancêtre_ d'un noeud tout noeud situé sur l'unique
  chemin entre ce noeud et la racine de l'arbre.
]

#ydef(name: [Descendant d'un noeud])[
  On dit que le noeud $d$ est un _descendant_ d'un noeud $a$
  si $a$ est un ancêtre de $d$.
]

#ydef(name: [Chemin dans un arbre])[
  Un _chemin_ d'un noeud $a$ à un noeud $b$ est
  une succéssion d'arcs permettant d'aller du
  noeud $a$ au noeud $b$.

  La _longueur_ d'un chemin est le nombre d'arcs empruntés par ce chemin.
]

#ydef(name: [Taille d'un arbre])[
  La _taille_ d'un arbre est le nombre de noeuds qui le compose.

  Elle est noté $abs(A)$ pour l'arbre $A$.
]

#ydef(name: [Hauteur d'un arbre])[
  La _hauteur_ d'un arbre est définie comme la longueur
  du plus long chemin entre la racine et un noeud de l'arbre.

  Un arbre réduit à un unique noeud a pour hauteur $0$.
]

#ydef(name: [Profondeur d'un noeud dans un arbre])[
  La _profondeur_ d'un noeud est la longueur du chemin de la racine à ce noeud.
]

#ydef(name: [Arbre binaire])[
  Un arbre binaire $A$ est
  - un _arbre vide_ noté $E$ ou $"Nil"$
  - un _noeud_ appelé _racine_, noté $x$, relié exctement à deux arbres
    binaires nommées _sous-arbre gauche_, noté $l$, et _sous arbre droit_, noté $r$.
]

#ydef(name: [Feuille d'un arbre binaire])[
  Une _feuille_ est un arbre du type
  $ A(E, x, E) $
]

#ydef(name: [Hauteur d'un arbre binaire])[
  La hauteur d'un arbre binaire $A$, notée $h(A)$, est définie par
  $ cases(h(E) = -1,
    h(A(l, x, r)) = 1 + max(h(l), h(r))) $
]

#ydef(name: [Taille d'un arbre binaire])[
  La taille d'un arbre binaire $A$, notée $abs(A)$, est définie par
  $ cases(abs(E) = 0,
    abs(A(l, x, r)) = 1 + abs(h(l)) + abs(h(r))) $
]

#ydef(name: [Rotation d'arbre binaire])[
  Pour un arbre binaire on définit deux rotation d'arbre.

  La _rotation à droite de centre $v$_ :
  #align(center)[
    #table(
      columns: 2,
      inset: 0.2em,
      stroke: none,
      figure(
        tree(($R$, ($v$, ($u$, $A_1$, $A_2$), $A_3$)), 1.2em),
        supplement: [Arbre],
        caption: [Avant]
      ),
      figure(
        tree(($R$, ($u$, $A_1$, ($v$, $A_2$, $A_3$))), 1.2em),
        supplement: [Arbre],
        caption: [Après]
      )
    )
  ]

  La _rotation à gauche de centre $u$_ :
  #align(center)[
    #table(
      columns: 2,
      inset: 0.2em,
      stroke: none,
      figure(
        tree(($R$, ($u$, $A_1$, ($v$, $A_2$, $A_3$))), 1.2em),
        supplement: [Arbre],
        caption: [Avant]
      ),
      figure(
        tree(($R$, ($v$, ($u$, $A_1$, $A_2$), $A_3$)), 1.2em),
        supplement: [Arbre],
        caption: [Après]
      )
    )
  ]
]

#ydef(name: [Arbre rouge-noir])[
  Un _arbre rouge-noir_ est un arbre binaire de recherche pour lequel
  chaque noeud porte une couleur (rouge ou noir) et pour lequel
  les propriétés suivantes sont vérifiées :

  - La racine est un noeud noir ;

  - les feuille sont des noeuds noirs ;

  - le père d'un noeud rouge n'est jamais un noeud rouge ;

  - pour chaque noeud, tous les chemins reliant ce noeud à des feuilles
    situées plus bas dans l'abre contiennent le même nombre de
    noeuds noirs.
]