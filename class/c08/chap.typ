// Induction structurelle
#import "../../template/ytemplate.typ": *

#ychap[Induction structurelle]

Une bonne partie des définitions sont issues de
#link("http://jdreichert.fr/Enseignement/CPGE/Cours/info_mp2i.pdf", [jdreichert.fr])

#include "part1.typ"
#include "part2.typ"