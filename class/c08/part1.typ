#import "../../template/ytemplate.typ": *


#ypart[Prérequis mathématiques]


#ydef(name: [Ordre / relation d'ordre])[
    Une relation binaire $R$ entre un ensemble $E$ et lui-même est un ordre
    (on dit aussi une relation d'ordre) sur $E$ si $R$ est réflexive, 
    antisymétrique et transitive.
    
    On dit alors que $(E, R)$ est un ensemble ordonné.
]

#ydef(name: [Ordre strict / relation d'ordre strict])[
    Soit $R$ un ordre sur l'ensemble $E$.

    $R$ est un ordre (ou une relation d'ordre) strict sur $E$ si
    l'on retire à $R$ l'ensemble des couples $(x, x)$ de $R$ pour $x in E$.
]

#ydef(name: [Ordre produit])[
    Soient deux ensembles ordonnés $(A, prec.eq)$ et $(B, prec.tilde)$.
    
    L'_ordre produit_ sur $A times B$ est l'ordre $<=$ tel
    que, pour $(a, b)$ et $(alpha, beta)$ dans $A times B$,
    $ (a, b) <= (alpha, beta) <=> cases(a prec.eq alpha, b prec.tilde beta) $

    L'ordre produit n'est pas total.
]

#ydef(name: [Ordre bien fondé])[
    Une relation d'ordre $prec.eq$ sur un ensemble $E$
    est _bien fondée_ s'il n'existe pas de suite infinie
    strictement décroissante dans $E$ pour $prec.eq$.
    
    Autrement dit, en notant $prec$ l'ordre strict associé à $prec.eq$,
    $ exists.not (x_k) in E^NN, forall k in NN, x_(k+1) prec x_k $
]

#ydef(name: [Bon ordre, ensemble bien ordonné])[
    Un ordre bien fondé pour lequel l'ordre est total
    est aussi appelé _bon ordre_ ; un ensemble
    muni d'un bon ordre est un _ensemble bien ordonné_.
]

#yprop(name: [Définition équivalente d'ensemble bien ordonné])[
    Un ensemble $(E, <=)$ est bien ordonné si, et seulement si,
    toute partie non vide de cet ensemble admet un élément minimal, _i.e._
    $ forall F in cal(P)(E) without {emptyset}, exists m in F,
        yt forall x in F, x lt.not m $
    $qed$
]

#ydef(name: [Ensemble et objet inductif])[
    Un _ensemble inductif_ est le plus petit ensemble engendré
    à partir d'_objets de base_ grâce à des régles appelées
    _règles de construction_ (ou d'_inférence_), qui permettent de
    construire des objets plus grands.

    Les éléments d'un tel ensemble sont appelés _objets inductifs_.
]

