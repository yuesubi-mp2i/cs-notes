#import "../../template/ytemplate.typ": *


#ypart[Objets inductifs]


#ydef(name: [Constructeur])[
    Un constructeur est un symbole attendant un nombre fixe d'arguments.
    Ce nombre est appelé _arité_ du constructeur.
]

#ydef(name: [Terme, sous-terme et construction de terme])[
    Un objet inductif $t$ est aussi appelé _terme_.
    Il est formé par un certain constructeur $c$ d'arité $n$ :
    $ t = c(t_1, ..., t_n, x_1, ..., x_p) $
    où les termes $t_1, ..., t_n$ sont appelés _sous-termes_ de $t$.
]

#ydef(name: [Terme ambigu, terme fini])[
    On dit qu'un terme est
    - _ambigü_ s'il peut être construit de plusieurs
        manières différentes ;
    - _fini_ s'il peut être construit avec un nombre fini
        d'applications de constructeurs.
]

#ydef(name: [Taille d'un objet inductif])[
    On dit qu'un objet inductif est _de taille $n$_ si
    il faut $n$ constructeurs pour le composer.

    N.B. cette taille peut être ambigüe.
]

#ytheo(name: [Théorème d'induction structurelle])[
    Soit $E$ un ensemble de termes, et $P$ une propriété
    concernant les objets de $E$.

    Si, pour chaque constructeur $c$ d'arité $n$,
    la propriété $P(c(t_1, ..., t_n))$ est satisfaite
    dès lors que les propriétés $P(t_1)$ à $P(t_n)$
    sont toutes satisfaites, alors, $P(t)$ est satisfaite
    pour tout $t in E$.

    N.B. l'énoncé est fourbe, car pour les cas de base,
    aucune hypothèse n'est supposée vraie pour que $P$ soit valide dessus.
]

#yproof[
    On note $E_n$ l'ensemble des élément $t in E$ construits
    avec un constructeur d'arité $n$.

    On pose
    $ A = { n in NN | exists t in E_n, not P(t) } $

    On suppose $A$ non vide, de plus $0 in.not A$.

    On pose $m = min(A)$, donc pour $t_F in E_m$, $P(t_F)$ est faux.

    De plus, pour tout $k in [|0, m - 1|] != emptyset$,
    pour $t in E_k$, $P(t)$ est vraie, car $k in.not A$.

    Donc $t_F$ est construit à partir de termes sur lesquels
    la propriété est vraie, donc $P(t_F)$ est vrai : une contradiction.

    Donc $A = emptyset$.
    $qed$
]

#yexample[
    - L'ordre lexicographique n'est pas bien fondé
    - L'ordre "est préfixe strict de ..." en est un
]

#ydef(name: [Ordre structurel])[
    Soit $E$ un ensemble inductif.
    On pose $<=$ la relation entre $E$ et lui-même de sorte
    que $x <= y$ si, et seulement si, y s'obtient à partir
    d'un constructeur dont $x$ est un des arguments.

    L'_ordre structurel_ sur $E$ est la relation $prec.eq$ telle que
    $x prec.eq y$ si, et seulement si, il existe une séquence finie
    $x_0, dots, x_n$ telle que $x_0 = x$, $x_n = y$ et pour tout
    $0 <= i < n$, on ait $x_i <= x_(i + 1)$.
]

#yprop(name: [Ordre structurel pour la terminaison])[
    L'ordre structurel est bien fondé.

    Toute fonction récursive sur une structure pour laquelle les appels 
    récursifs ont uniquement pour arguments des éléments strictement
    inférieurs, selon l'ordre structurel, à l'argument de l'appel en
    cours termine.
]