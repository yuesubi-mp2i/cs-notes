#import "../template/ytemplate.typ": *


#set page(
    // header: locate(loc => {
    //     [pass]
    // }),
    width: 1080pt,
    height: 8640pt,
    margin: (x: 20pt, y: 20pt)
)

#set text(size: 62pt)

#align(center, text(size: 2em)[*Notes de maths de MP2I*])

#outline(title: [Sommaire], depth: 2)

// #pagebreak()


#show: doc => yconf_math(yconf_numering(doc))

#include "complete.typ"