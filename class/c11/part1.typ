#import "../../template/ytemplate.typ": *


#ypart[Algos de tri]


#let tri_insertion() = {
    pseudocode-list[
        - *Entrées:* tableau $t$ de taille $n in NN$
        - *Post-condition:* $t$ est trié

        + *pour* $i <- 1$ *à* $n$ *faire*
            + $"clé" <- t[i]$
            + $j <- i$
            + *tant que* $j > 1$ *et* $"clé" < t[j - 1]$ *alors*
                + $t[j] <- t[j - 1]$
                + *décrémenter* $j$
            + *fin*
            + $t[j] <- "clé"$
        + *fin*
    ]
}


#yalgo(name: [Tri par insertion])[
    #tri_insertion()
]

#yprop(name: [Complexité du tri par insertion])[
    Complexité en $Theta(n^2)$.
]

#yproof[
    - La boucle *pour* fait $n$ itérations
        - La boucle *tant que* fait au maximum
            $i - 1$ itérations, soit $i$ comparaisons.

            Et ce maximum est atteint si le
            tableau $t$ était initialement
            trié dans l'ordre strictement
            décroissant.
    
    Donc la complexité $C(n)$ est majorée par
    $ sum_(i=1)^n i &= (n (n+1))/2 = 1/2 n^2 + 1/2 n \
        &= O(n^2) $
    et ce majorant est atteint, donc
    $ C(n) = Theta(n^2) $

    #tri_insertion()
]

#yprop(name: [Variant et invariants du tri par insertion #tri_insertion()])[
    Variant : $j$

    Invariants
    - boucle *pour* : $t[1:i-1]$ est trié
        dans l'ordre croissant
    - boule *tant que* :
        $ forall k in [|j + 1, i|], "clé" < t[k] $
]


#let tri_selec() = {
    pseudocode-list[
        - *Entrées:* tableau $t$ de taille $n in NN$
        - *Post-condition:* $t$ est trié

        + *pour* $i <- 1$ *à* $n$ *faire*
            + $k <- i$
            + *pour* $j <- i + 1$ *à* $n$ *faire*
                + *si* $t[j] < t[k]$ *alors*
                    + $k <- j$
                + *fin*
            + *fin*
            + $"échanger"(t, i, k)$
        + *fin*
    ]
}


#yalgo(name: [Tri par sélection])[
    #tri_selec()
]

#yprop(name: [Complexité du tri par sélection])[
    Complexité en $Theta(n^2)$.
]

#yproof[
    Il y a
    $ sum_(i=1)^n sum_(j = i+1)^n 1
        &= sum_(i=1)^n (n - i) \
        &= (n (n - n  + n - 1))/2 \
        &= Theta(n^2) $
    comparaisons.

    #tri_selec()
]

#yprop(name: [Invariants du tri par sélection #tri_selec()])[
    Invariants
    - boule externe :
        $ &forall k in [|1, i-1|], forall ell in [|i, n|], \
            &wide t[k] <= t[ell] $
    - boucle interne :
        $ forall ell in [|i, j-1|], t[k] <= t[ell] $
]


#let tree(data, length) = {
  cetz.canvas(length: length, {
    import cetz: *
    import cetz.draw: *

    set-style(content: (padding: .2),
      // fill: gray.lighten(70%),
      stroke: (paint: gray.lighten(70%), thickness: 0.06em))

    tree.tree(data, spread: 2.5, grow: 1.5, draw-node: (node, ..) => {
      circle((), radius: .45, stroke: none)
      content((), node.content)
    }, draw-edge: (from, to, ..) => {
      line((a: from, number: .5, b: to),
           (a: to, number: .5, b: from))
    }, name: "tree")

    // Draw a "custom" connection between two nodes
    // let (a, b) = ("tree.0-0-1", "tree.0-1-0",)
    // line((a, .6, b), (b, .6, a), mark: (end: ">", start: ">"))
  })
}

#let trifus_tri() = {
    algorithm(
        caption: [tri],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Sortie:* le tableau $t$ trié

            + *si* $n <= 1$ *alors*
                + *retourner* t
            + *fin*
            + $(t_1, t_2) <- "partage"(t)$
            + $t'_1 <- "tri"(t_1)$
            + $t'_2 <- "tri"(t_2)$
            + *retourner* $"fusion"(t'_1, t'_2)$
        ]
    )
}


#yalgo(name: [Tri fusion])[
    #trifus_tri()
]

#yprop(name: [Complexité du tri fusion])[
    Complexité en $Theta(n ln(n))$.
]

#yproof[
    #underline[Idée intuitive] :
    L'arbre des appels récursifs de fusion est affiché
    ci-dessous.
    
    La complexité de l'opération effectuée
    à chaque étape est affichée sur le noeud,
    et correspond,
    - pour l'arbre du haut, à l'appel de partage
    - et, pour l'arbre du bas, à l'appel de fusion.

    #align(center)[
        #cetz.canvas(length: 1.2em, {
            import cetz: *
            import cetz.draw: *

            set-style(
                content: (padding: .2),
                // fill: gray.lighten(70%),
                stroke: (
                    paint: gray.lighten(70%),
                    thickness: 0.06em))

            tree.tree(
                ($n$,
                    ($n/2$,
                        ($n/4$, $$, $$),
                        ($n/4$, $$, $$)),
                    ($n/2$,
                        ($n/4$, $$, $$),
                        ($n/4$, $$, $$))),
                spread: 1.8,
                grow: 2,
                draw-node: (node, ..) => {
                    circle((), radius: .45, stroke: none)
                    content((), node.content) },
                draw-edge: (from, to, ..) => {
                    line(
                        (a: from, number: .75, b: to),
                        (a: to, number: .5, b: from),
                        stroke: (dash:
                            if from.len() == 6 { "densely-dashed" }
                            else { none })) },
                name: "tree")
        })

        #v(-1.5em)

        #let truc() = {
            $1 space$ * 17
            $...$
            $space 1$
        }
        truc()

        #v(-1.5em)

        #cetz.canvas(length: 1.2em, {
            import cetz: *
            import cetz.draw: *

            set-style(
                content: (padding: .2),
                // fill: gray.lighten(70%),
                stroke: (
                    paint: gray.lighten(70%),
                    thickness: 0.06em))

            tree.tree(
                ($n$,
                    ($n/2$,
                        ($n/4$, $$, $$),
                        ($n/4$, $$, $$)),
                    ($n/2$,
                        ($n/4$, $$, $$),
                        ($n/4$, $$, $$))),
                spread: 1.8,
                grow: 2,
                draw-node: (node, ..) => {
                    circle((), radius: .45, stroke: none)
                    content((), node.content) },
                draw-edge: (from, to, ..) => {
                    line(
                        (a: from, number: .75, b: to),
                        (a: to, number: .5, b: from),
                        stroke: (dash:
                            if from.len() == 6 { "densely-dashed" }
                            else { none })) },
                name: "tree",
                direction: "up")
        })
    ]

    La complexité de chaque niveau de l'arbre est de
    l'ordre de $Theta(n)$ et il y a de l'ordre de
    $Theta(ln(n))$ niveaux.

    La complexité finale est donc de l'ordre de $Theta(n ln(n))$.

    (N.B. si on dit explicitement qu'on s'intéresse au
    nombre de comparaisons, la partie partage est en $Theta(1)$,
    mais ça ne change pas le résultat)

    #underline[Majoration] :
    La complexité de l'algorithme est donnée
    par la formule récurente suivante :
    - $C(0) = 0$ (pas très utile)
    - $C(1) = 0$
    - pour $n >= 2$, \
        $C(n) = n + cases(2 C(n/2) &"si" n "est pair",
                C((n+1)/2) + C((n-1)/2) &"sinon") $
    
    On pose, pour $n in NN^*$,
    $ P(n) : C(n) <= 2n log_2 (n) $
    
    - _Initialisation_ :
        $ C(1) = 0 <= 0 = 2 times 1 times log_2(1) $
    
    - _Hérédité_ : Soit $n in NN^*$. On suppose que
        $ forall k in [|1, n|], P(k) $

        - Cas 1 : on suppose $n + 1$ pair.
            $ &C(n+1) \
                &= n + 1 + 2 C((n + 1)/2) \
                &<= n + 1 + 4 (n+1)/2 log_2 ((n + 1)/2) \
                &<= 2(n + 1) log_2 (2) \
                    &wide + 2(n + 1) log_2((n + 1)/2) \
                &<= 2(n + 1) log_2 (n + 1) $

        - Cas 2 : on suppose $n + 1$ impair.
            $ &C(n + 1) \
                &= n + 1 + C((n + 2)/2) + C(n/2) \
                &<= n + 1 + 2 (n + 2)/2 log_2 (n/2 + 1) \
                    &wide 2 n/2 log_2(n/2) \
                &<= n + 1 + (n + 2) log_2 (n/2 + 1) \
                    &wide n log_2(n/2 + 1) \
                &<= n + 1 + 2(n + 1) log_2 (n/2 + 1) \
                &<= 2(n + 1) log_2(sqrt(2)) \
                    &wide + 2(n + 1) log_2 (n/2 + 1) \
                &<= 2(n + 1) log_2 (sqrt(2)(n/2 + 1)) \
                &<=^1 2(n + 1) log_2 (n + 1) $
    
    $space^1$ car $log_2$ est croissant et
    $ &sqrt(2) (n/2 + 1) <= n + 1 \
        &<=> n + 2 <= sqrt(2) n + sqrt(2) \
        &<=> 2 - sqrt(2) <= n(sqrt(2) - 1) \
        &<=> (2 - sqrt(2))/(sqrt(2) - 1)
            = sqrt(2) (sqrt(2) - 1)/(sqrt(2) - 1) <= 2 <= n $

    #trifus_tri()
]


#let trifus_part() = {
    algorithm(
        caption: [partage],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Sortie:* deux tableaux $(t_1, t_2)$ contenant chaqu'un
                la moitié des éléments de $t$

            + $m <- floor(n slash 2)$
            + $t_1 <- t[1..m]$
            + $t_2 <- t[(m+1)..n]$
            + *retourner* $(t_1, t_2)$
        ]
    )
}


#yalgo(name: [Fonction partage du tri fusion])[
    #trifus_part()
]

#yprop(name: [Complexité de la Fonction partage du tri fusion])[
    Complexité en $Theta(n)$ (ou en $Theta(1)$
    si on s'intéresse qu'aux comparaisons).
]

#yproof[
    Ça se voit.
    #trifus_part()
]


#let trifus_fus_func() = {
    algorithm(
        caption: [fusion],
        pseudocode-list[
            - *Entrées:* deux tableaux $t_1, t_2$ triés
            - *Sortie:* un tableau $t$ triés contenant les éléments
                de $t_1$ et $t_2$
            + *match* $(t_1, t_2)$ *avec*
                + *cas* $([], t)$ *ou* $(t, [])$ *alors*
                    + $t$
                + *cas* $(h_1 :: ell_1, h_2 :: ell_2)$ *alors*
                    + *si* $h_1 <= h_2$ *alors*
                        + $h_1 :: "fusion"(ell_1, t_2)$
                    + *sinon*
                        + $h_2 :: "fusion"(t_1, ell_2)$
        ]
    )
}

#let trifus_fus_imp() = {
    algorithm(
        caption: [fusion (impératif)],
        pseudocode-list[
            - *Entrées:* deux tableaux $t_1, t_2$ triés de taille
                $n_1, n_2 in NN$
            - *Sortie:* un tableau $t$ triés contenant les éléments
                de $t_1$ et $t_2$

            + $t <- "créer_tableau"(n) $
            + $i_1 <- 1$
            + $i_2 <- 1$
            + *tant que* $i_1 <= n_1$ *and* $i_2 <= n_2$ *faire*
                + $i <- i_1 + i_2$
                + *si* $i_1 > n_1$ *alors*
                    + $t[i] <- t_2[i_2]$
                    + $i_2 <- i_2 + 1$
                + *sinon si* $i_2 > n_2$ *ou* $t_1[i_1] <= t_2[i_2]$ *alors*
                    + $t[i] <- t_1[i_1]$
                    + $i_1 <- i_1 + 1$
                + *sinon*
                    + $t[i] <- t_2[i_2]$
                    + $i_2 <- i_2 + 1$
                + *fin*
            + *fin*
            + *retourner* $(t_1, t_2)$
        ]
    )
}

#yalgo(name: [Fonction fusion du tri fusion])[
    #trifus_fus_func()
]

#yprop(name: [Complexité de la fonction fusion du tri fusion])[
    Complexité en $Theta(n)$ où $n = abs(t_1) + abs(t_2)$.
]

#yproof[
    On majore aisément la complexité, et
    celle si est atteinte avec les tableaux
    $ t_1 = [0, 2, 4, ..., floor(n / 2)] $
    $ t_2 = [1, 3, 5, ..., floor((n+1) / 2)] $

    #trifus_fus_func()
]


#let trirap() = {
    algorithm(
        caption: [tri],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Sortie:* le tableau $t$ trié

            + *si* $n <= 1$ *alors*
                + *retourner* t
            + *fin*
            + $(p, t_1, t_2) <- "partage"(t)$
            + $t'_1 <- "tri"(t_1)$
            + $t'_2 <- "tri"(t_2)$
            + *retourner* $t'_1 union [p] union t'_2 $
        ]
    )
}

#yalgo(name: [Tri rapide])[
    #trirap()
]

#yprop(name: [Complexité du tri rapide])[
    Complexité en $Theta(n^2)$.
]

#yproof[
    La complexité maximale est atteinte
    pour un tableau initialement trié.

    #trirap()
]


#let trirap_part() = {
    algorithm(
        caption: [partage],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Sortie:* un élément et deux tableaux $("clé", t_1, t_2)$ 
            - *Pré-condition:* $n != 0$
            - *Post-conditions:*
                - $"clé" = t[0]$
                - $t_1$ contient seulement les élément de $t[2..n]$
                    strictement inférieurs à $"clé"$
                - $t_2$ contient seulement les élément de $t[2..n]$
                    suppérieurs à $"clé"$

            + $"clé" <- t[0]$
            + $t_1 <- []$
            + $t_2 <- []$
            + *pour* $i <- 2$ *à* $n$ *faire*
                + *si* $t[i] < "clé"$ *alors*
                    + $"ajouter"(t_1, t[i])$
                + *sinon*
                    + $"ajouter"(t_2, t[i])$
                + *fin*
            + *fin*
            + *retourner* $("clé", t_1, t_2)$
        ]
    )
}

#yalgo(name: [Fonction partage du tri rapide])[
    #trirap_part()
]

#yprop(name: [Complexité de la fonction partage du tri rapide])[
    Complexité en $Theta(n)$.
]

#yproof[
    C'est un boucle *pour* de $n-1$ itérations.

    #trirap_part()
]


#let tritas() = {
    algorithm(
        caption: [tri],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Post-condition:* le tableau $t$ est trié
            + $"construire_tas_max"(t)$ 
            + *pour* $k <- n$ *à* $2$ *faire*
                + *échanger* $t[1]$ *avec* $t[k]$
                + $"reconstruire_tas"(t, k, 1)$
                + #comment[reconstruire tout le tas max $t$
                    qui est de taille $k$]
            + *fin*
        ]
    )
}

#yalgo(name: [Tri par tas])[
    #tritas()
]

#yprop(name: [Complexité du tri par tas])[
    Complexité en $Theta(n ln(n))$.
]

#yproof[
    $"construire_tas_max"$ est en $Theta(n ln n)$

    Et il y a $n - 1$ itérations de $"reconstruire_tas"$
    qui est en $Theta(ln n)$.

    #tritas()
]


#let tritas_constr() = {
    algorithm(
        caption: [construire_tas_max],
        pseudocode-list[
            - *Entrées:* tableau $t$ de taille $n in NN$
            - *Post-conditions:*
                - $t$ est un tas max équilibré
                - $t$ contient les mêmes éléments qu'initialement
            + *pour* $i <- floor(n slash 2)$ *à* $1$ *faire*
                + $"entasser"(t, n, i)$
            + *fin*
        ]
    )
}

#yalgo(name: [Fonction construire_tas_max du tri par tas])[
    #tritas_constr()
]

#yprop(name: [Complexité de la fonction construire_tas_max du tri par tas])[
    Complexité en $Theta(n ln(n))$.
]

#yproof[
    Il y a $n slash 2$ itérations d'entasser qui
    est en $Theta(ln n)$.

    #tritas_constr()
]

#yalgo(name: [Fonction entasser du tri par tas])[
    _À supprimer_
]

#let tritas_reconstr() = {
    algorithm(
        caption: [reconstruire_tas],
        pseudocode-list[
            - *Entrées:* un tableau $t$ stockant un tas équilibré de
                taille $n in NN$ et un indice $i$
            - *Pré-condition:* Les sous arbres de $i$ sont des tas max
            - *Post-condition:* $i$ est un tas max équilibré

            + $g <- "gauche"(i)$ #comment[$"gauche"(i) = 2i$]
            + $d <- "droite"(i)$ #comment[$"droite"(i) = 2i + 1$]
            + *si* $g <= n$ *et* $t[i] > t[g]$ *alors*
                + $min <- g$
            + *sinon* *si* $d <= n$ *et* $t[min] > t[d]$ *alors*
                + $min <- d$
            + *sinon*
                + $min <- i$
            + *fin*
            + *si* $min != i$ *alors*
                + *échanger* $t[i]$ *avec* $t[min]$
            + *fin*
        ]
    )
}

#yalgo(name: [Fonction reconstruire_tas du tri par tas])[
    #tritas_reconstr()

    N.B. la prof appelle cette fonction « entasser ».
]

#yprop(name: [Complexité de la fonction reconstruire_tas du tri par tas])[
    Complexité en $Theta(ln(n))$.
]

#yproof[
    On descent un arbre équilibré de taille $n$.

    #tritas_reconstr()
]