#import "../../template/ytemplate.typ": *


#ypart[Lempel-Ziv-Welch]


#let lzw() = {[Lempel-Ziv-Welch]}


#ydef(name: [Orthographe de LZW])[
  "#lzw()" de ses inventeurs :
  - Abraham Lempel,
  - Jacob Ziv
  - et Terry Welch.
]

#ydef(name: [Fun facts sur l'algo de LZW])[
  #lzw()
  - Compression sans pertes ;
  - le plus souvent pas optimal ;
  - aucun traitement préalable ;
  - efficace sur les textes.
]

#yalgo(name: [Idée de encodage des mots dans LZW])[
  Dans l'algorithme de #lzw(),
  on code les mots sur 12 bits
  - Les codes de 0 à 255 correspondent
    aux caractères ASCII.
  - Les codes 256 et 267 sont parfois
    utilisés pour indiquer la fin du
    fichier ou la remise à zéro de certains
    mots du dictionnaire.
  - Les autres codes correspond à des mots
    que l'on ajoute au dictionnaire au
    fur et à mesure.
]

#yalgo(name: [Algorithme de LZW])[
  #lzw()
  #pseudocode-list[
    - *Entrées:* chaîne de caractère $s$
    - *Sortie:* code de LZW associé à $s$

    + $m <- $ ""  #comment[le motif]
    + $d <- $ dictionnaire de la table ascii
    + $c <- []$  #comment[le code final]

    + *tant que* $ell <- "prochain"(s)$
      + $n <- "concat"(m, ell)$  #comment[nouveau motif]
      + *si* $n in d$
        + $m <- n$  #comment[peut-être qu'un motif
          plus long est dans $d$, donc on encode
          pas tout de suite]
      + *sinon*
        + $"créer_code"(d, n)$
        + $"ajouter"(c, m)$
        + $m <- ell$  #comment[réinitialisation de $m$]

    + *retourner* c
  ]
]