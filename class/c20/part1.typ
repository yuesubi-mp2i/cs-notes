#import "../../template/ytemplate.typ": *


#ypart[Huffman]


#let tree(data, length) = {
  cetz.canvas(length: length, {
    import cetz: *
    import cetz.draw: *

    set-style(content: (padding: .2),
      // fill: gray.lighten(70%),
      stroke: (paint: gray.lighten(70%), thickness: 0.06em))

    tree.tree(data, spread: 2.5, grow: 1.5, draw-node: (node, ..) => {
      circle((), radius: .45, stroke: none)
      content((), node.content)
    }, draw-edge: (from, to, ..) => {
      line((a: from, number: .5, b: to),
           (a: to, number: .5, b: from))
    }, name: "tree")
  })
}


#ydef(name: [Taux de compression, économie d'espace])[
  Soit un texte de $n$ bits que l'on a
  compressé en un texte de $c$ bits.

  On définit alors,
  - le _taux de compression_ comme
    le rapport $n/c$ ;
  - l'_économie d'espace_ comme
    la valeur $E = 1 - c/n$.
]

#ydef(name: [Codage, mot code])[
  Soit $Sigma$ un alphabet.

  On appelle
  - _codage_ sur $Sigma$ une application $c$ de $Sigma$
    dans l'ensemble des mots binaires ;
  - _mot code_ de $a in Sigma$
    le mot binaire $c(a)$.
]

#ydef(name: [Codage préfixe, code préfixe])[
  Un _codage préfixe_ (ou _code préfixe_)
  est un codage pour lequel aucun mot code
  n'est le préfixe d'un autre mot code.
]

#yprop(name: [Caractérisation des codages préfixes par des arbres])[
  Un codage est préfixe ssi il existe un
  arbre de mots binaire tel que tous les mots codes
  sont des feuilles de l'arbre.

  #align(center)[
    #figure(
      tree(
        ($epsilon$,
          ($0$, $underbrace(00, c(alpha))$),
          ($1$,
            $underbrace(10, c(gamma))$,
            ($11$, $underbrace(111, c(beta))$)
          )
        ),
        2em
      ),
      supplement: [Exemple],
      caption: [Codage préfixe sur
        l'alphabet $Sigma = {alpha, beta, gamma}$]
    )
  ]

  #align(center)[
    #figure(
      tree(
        ($epsilon$,
          ($0$, $underbrace(00, c(alpha))$),
          ($underbrace(1, c(gamma))$,
            ($11$, $underbrace(111, c(beta))$)
          )
        ),
        2em
      ),
      supplement: [Exemple],
      caption: [Codage non-préfixe sur
        l'alphabet $Sigma = {alpha, beta, gamma}$]
    )
  ]
]

#yalgo(name: [Construction de l'arbre de Huffman])[
  - On a un texte
    ```
    satisfaisant
    ```

  - On écrit le nombre d'occurrances
    de chacune des lettres :
    ```
    a(3)  s(3) f(1) n(1) i(2) t(2)
    ```
  
  - On met les deux caractères d'occurrances
    les plus faibles ensemble :
    ```
    a(3)  s(3)    (2)    i(2) t(2)
                  / \
               f(1) n(1)
    ```
  
  - On recommence
    ```
    a(3)  s(3)    (2)       (4)
                  / \       / \
               f(1) n(1) i(2) t(2)
    ```
  
  ...
  
  - On obtient alors
    ```
             (12)
            /    \
         (5)       (7)
         / \       / \
       (2) a(3) s(3) (4)
       / \           / \
    f(1) n(1)     i(2) t(2)
    ```
  
  $->$ On le code avec une file de priorité

  $->$ Complexité en $O(|Sigma| ln|Sigma|)$
    car $|Sigma|$ opérations sur une file
    de priorité.
]

#ytheo(name: [Optimalité de l'arbre de Huffman])[
  L'arbre construit par l'algorithme de Huffman
  minimise la quantitée
  $ S = sum_(a in Sigma) f_a d_a $
  où
  - $d_a$ est la profondeur du
    caractère $a$ dans l'arbre i.e.
    la longeur du mot code de $a$ ;
  - $f_a$ est la fréquence du caractère $a$.
]

#yproof[
  Par l'absurde (en exercice).
]