#import "../../template/ytemplate.typ": *


#ypart[Graphes orientés]


#ydef(name: [Graphe orienté])[
	Un graphe orienté $G = (S, A)$ est constitué de :
	- un ensemble fini $S$ de _sommets_ ;
	- un ensemble $A subset.eq S times S$ de couples de sommets
		appelés _arcs_.
]

#ynot(name: [Arc])[
	Soit $(S, A)$ un graphe orienté.

	(...)
]

#digraph(
		```dot
			a -> b
			a -> c
			b -> c
			b -> d
			c -> d
			d -> c
			d -> d

			e->f
			f-> e

			g
		```
	)