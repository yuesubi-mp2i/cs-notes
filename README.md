# MP2I ~~Math~~ CS Notes
My ~~math~~ cs class notes of the year.
All the notes are written in [Typst](https://typst.app).

__~~CHECK OUT the~~ No generated PDF :__
- ~~✨ [math_notes.pdf](https://gitlab.com/api/v4/projects/51473655/jobs/artifacts/main/raw/math_notes.pdf?job=normal-pdf) ✨~~
- ~~✨ [math_notes_webtoon.pdf](https://gitlab.com/api/v4/projects/51473655/jobs/artifacts/main/raw/math_notes_webtoon.pdf?job=webtoon-pdf) ✨~~


~~__Get the Anki decks :__ [BROWSE](/anki-index.md)~~


## Generating Anki decks
~~The python script to generate Anki decks is still really sketchy, I'll fix it eventually when I get the time to do so.~~
I'll probably newer fix the anki generation script