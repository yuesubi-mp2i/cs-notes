#set text(
    fill: rgb("#ebdbb2")
)

#set page(
    width: 17.5em,
    height: auto,
    margin: (x: 0.3em, top: 0.3em, bottom: 1em),
    fill: rgb("#282828")
)


#import "../template/ytemplate.typ": yconf_math
#show: yconf_math