#let yblock(kind: [], name: [], content) = {[
    ==== #smallcaps(kind) : #name
    #par(justify: true)[ #content ]
]}


#let ydef(name: [], content) = {[
    #yblock(
        kind: [ #underline[Définition] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ynot(name: [], content) = {[
    #yblock(
        kind: [ #underline[Notation] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ytheo(name: [], content) = {[
    #yblock(
        kind: [ #underline[Théorème] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let yprop(name: [], content) = {[
    #yblock(
        kind: [ #underline[Proposition] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ycor(name: [], content) = {[
    #yblock(
        kind: [ #underline[Corollaire] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ylemme(name: [], content) = {[
    #yblock(
        kind: [ #underline[Lemme] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ymetho(name: [], content) = {[
    #yblock(
        kind: [ #underline[Méthode] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let yalgo(name: [], content) = {[
    #yblock(
        kind: [ #underline[Algorithme] ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let yproof(content) = {[
    #yblock(
        kind: [ #underline[Preuve] ],
        name: []
    )[
        #content
    ]
]}


#let yexample(name: [], content) = {[
    #yblock(
        kind: [ Exemple ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let yexercise(name: [], content) = {[
    #yblock(
        kind: [ Exercice ],
        name: [ #name ]
    )[
        #content
    ]
]}


#let ybtw(name: [], content) = {[
    #yblock(
        kind: [ Remarque ],
        name: [ #name ]
    )[
        #content
    ]
]}