#import "@preview/cetz:0.2.2"
#import "@preview/lovelace:0.2.0" : *
#import "@preview/diagraph:0.2.2" : render


#let digraph(code) = {
    let head = ```
    digraph {
        node[shape=none]
        node[color=white]
        edge[color=white]
        edge[arrowsize=0.3]
        edge[len=1]
    ```
    
    let tail = ```
	}
    ```
    
	render(
        width: 16em,
        engine: "circo",
        head.text + code.text + tail.text
    )
}


#let yconf_math(doc) = {
    show math.equation.where(block: true): mathblock => {
        set align(left)
        pad(left: 8%, mathblock)
    }

    setup-lovelace(doc)
}


#let ymega_emph(doc) = text(gradient.linear(..color.map.rainbow), weight: "black")[#underline[#emph[#doc]]]


#let _y_tab_trans_circ = text(fill: rgb("#00000000"), sym.circle.filled.tiny)
#let _y_tab = {
    _y_tab_trans_circ * 2
    sym.circle.filled.tiny
    _y_tab_trans_circ * 2
}

#let yt = $\ #_y_tab$
#let ytt = $yt #_y_tab$
#let yttt = $ytt #_y_tab$
#let ytttt = $yttt #_y_tab$
#let yttttt = $ytttt #_y_tab$

#let ytttttt = $yttttt #_y_tab$
#let yttttttt = $ytttttt #_y_tab$
#let ytttttttt = $yttttttt #_y_tab$
#let yttttttttt = $ytttttttt #_y_tab$
#let ytttttttttt = $yttttttttt #_y_tab$