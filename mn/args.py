import argparse
import dataclasses
import os

from argparse import ArgumentParser
from dataclasses import dataclass
from typing import Union


class Prog:
    NAME: str = os.path.basename(__file__)
    DESC: str = """Start typing cs quickly!"""


class Args:

    @dataclass
    class Anki:
        chapter: int
        part_amount: int

    @dataclass
    class Apkg:
        chapter: int

    @dataclass
    class Class:
        chapter: int
        part: int

    @dataclass
    class Complete:
        pass
    
    @dataclass
    class Draft:
        name: str
    
    @dataclass
    class Td:
        chapter: str
        exercise: str
    
    SubCmd = Union[Anki, Apkg, Class, Complete, Draft, Td]

    @classmethod
    def parse(cls) -> SubCmd:
        parser = ArgumentParser(
            prog=Prog.NAME,
            description=Prog.DESC
        )
        cls._config_subparsers(parser)
        args = parser.parse_args()
        return cls._args_to_dataclass(args)
    
    @classmethod
    def _config_subparsers(cls, prog_parser: ArgumentParser):
        sub_parser_action = prog_parser.add_subparsers(title="subcommands")
        sub_cmd_types = cls.SubCmd.__args__

        for cmd_type in sub_cmd_types:
            name = cmd_type.__name__.lower()

            parser = sub_parser_action.add_parser(
                name,
                help=f"Type `{Prog.NAME} {name} -h` for help with {name}",
            )

            for field in dataclasses.fields(cmd_type):
                parser.add_argument(field.name, type=field.type)
            
            parser.set_defaults(cmd_type=cmd_type)
    
    @classmethod
    def _args_to_dataclass(cls, args: argparse.Namespace) -> SubCmd:
        cmd = args.cmd_type.__new__(args.cmd_type)
        args_dict = vars(args)

        for field in dataclasses.fields(args.cmd_type):
            cmd.__dict__[field.name] = args_dict[field.name]

        return cmd